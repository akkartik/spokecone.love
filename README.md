# spokecone.love

A tool for manually and painstakingly creating debug experiences tailored to a
specific domain and program. Get on the road to learning to [reinvent wheels](https://handmade.network/jam)
for [bicyles of the mind](https://merveilles.town/@akkartik/108607458977376162).
([video demo; 30 seconds](https://handmade.network/snippet/1533))

Concretely, spokecone.love is an editor for Lua programs written in a certain
style amenable to debug by print. The programs are plain text, but with a
twist. Every line has an optional _fold_, a special character. Spokecone.love
renders such _bifold_ text to show just text before the fold (normal text)
by default. Text after the fold (debug prints) can be shown on demand. Evolve
your debug prints with your program while keeping programs looking clean and
short by default. The debug prints will still execute.

When you open a log generated from the debug prints, spokecone.love opens in
_tenon auger_ mode. Logs are rendered read-only, but can be rendered in
arbitrarily sophisticated graphical ways as client programs specify.

spokecone.love is a fork of [lines.love](http://akkartik.name/lines.html),
an editor for plain text where you can also seamlessly insert line drawings.
Designed above all to be easy to modify and give you early warning if your
modifications break something.

([Names inspired by modern wheelwrights working to keeping their craft alive.](https://www.youtube.com/watch?v=mOXT3dFFOPU))

## Quick start

Install [LÖVE](https://love2d.org). It's just a 5MB download. I'll assume
below that you can invoke it using the `love` command.

Download this repo and the example codebase:

```
git clone https://codeberg.org/akkartik/spokecone.love spokecone
git clone https://codeberg.org/akkartik/spokecone-pong.love pong
```

Run the editor on the example codebase:
```
love spokecone pong/main.splua
```

Check out the keyboard shortcuts below to see what you can do.

Close the editor when you're done. (It'll autosave.)

Build the example program. This requires an extra step:

```sh
cd pong
./build  # unfortunately Unix only for now; PRs welcome
cd ..
```

Run the example program:

```
love pong
```

Play a few games. It'll generate a file called `log` in the current directory.

Open `log` using spokecone:

```
love spokecone log
```

Try clicking on log lines to expand them or open the source code that
generated them.

## Invocation

To run from the terminal, [pass this directory to LÖVE](https://love2d.org/wiki/Getting_Started#Running_Games).

Arguments in _spoke cone_ (edit) mode:
  * source file to edit

Arguments in _tenon auger_ (log browser) mode:
  * log file to parse and browse read-only
  * (optional) a source file containing lua definitions in namespace
    `debug_log_render` that will be used to render log lines. For example:

    ```
    debug_log_render = {}

    function debug_log_render.state(obj, x,y, w)
      assert(obj.name == 'state')
      local h = 30
      love.graphics.rectangle('line', x,y, w,h)
      return h
    end
    ```

    This definition will render log lines of this form:

    ```
    main.lua:50: {"name": "state", ...}
    ```

    as rectangles 30 pixels tall.

    Definitions return the height they need to render their argument. Tenon
    auger will pass in a non-overlapping `y` to the next line.

optionally with a file path to edit.

Alternatively, turn it into a .love file you can double-click on:
```
$ zip -r /tmp/spokecone.love *.lua
```

By default, it reads/writes the file `lines.txt` in your default
user/home directory (`https://love2d.org/wiki/love.filesystem.getUserDirectory`).

To open a different file, drop it on the app window.

## Keyboard shortcuts

* `ctrl+b` to expand text after the fold (the green squares)
* `ctrl+d` to edit text after the fold on a line

* `ctrl+f` to find patterns within a file
* `ctrl+z` to undo, `ctrl+y` to redo
* `ctrl+=` to zoom in, `ctrl+-` to zoom out, `ctrl+0` to reset zoom
* `alt+right`/`alt+left` to jump to the next/previous word, respectively

Exclusively tested so far with a US keyboard layout. If
you use a different layout, please let me know if things worked, or if you
found anything amiss: http://akkartik.name/contact

## Known issues

* No support yet for Unicode graphemes spanning multiple codepoints.

* No support yet for right-to-left languages.

* Undo/redo may be sluggish in large files. Large files may grow sluggish in
  other ways. Works well in all circumstances with files under 50KB.

* If you kill the process, say by force-quitting because things things get
  sluggish, you can lose data.

* Long wrapping lines can't yet distinguish between the cursor at end of one
  screen line and start of the next, so clicking the mouse to position the
  cursor can very occasionally do the wrong thing.

* No scrollbars yet. That stuff is hard.

## Mirrors and Forks

This repo is a fork of lines.love at [http://akkartik.name/lines.html](http://akkartik.name/lines.html).
Its immediate upstream is [text.love](https://codeberg.org/akkartik/text.love),
a version without support for line drawings. Updates to it can be downloaded
from:

* https://codeberg.org/akkartik/spokecone.love

Further forks are encouraged. If you show me your fork, I'll link to it here.

## Feedback

[Most appreciated.](http://akkartik.name/contact)
