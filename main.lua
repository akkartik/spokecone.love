utf8 = require 'utf8'
json = require 'json'

require 'app'
require 'test'

require 'keychord'
require 'button'

require 'main_tests'

-- delegate most business logic to a layer that can be reused by other projects
require 'edit'
Editor_state = {}

require 'tenonauger'

-- called both in tests and real run
function App.initialize_globals()
  -- tests currently mostly clear their own state

  Show = {}
  Focus = 'spokecone'

  -- a few text objects we can avoid recomputing unless the font changes
  Text_cache = {}

  -- blinking cursor
  Cursor_time = 0

  -- for hysteresis in a few places
  Last_resize_time = App.getTime()
  Last_focus_time = App.getTime()  -- https://love2d.org/forums/viewtopic.php?p=249700
end

-- called only for real run
function App.initialize(arg)
  love.keyboard.setTextInput(true)  -- bring up keyboard on touch screen
  love.keyboard.setKeyRepeat(true)

  love.graphics.setBackgroundColor(1,1,1)

  if love.filesystem.getInfo('config') then
    load_settings()
  else
    initialize_default_settings()
  end

  if #arg == 0 then
    initialize_spokecone(arg)
  else
    local type = detect_filetype(arg[1])
    if type == 'spokecone' then
      initialize_spokecone(arg)
      Show.spokecone = true
    else
      initialize_tenonauger(arg)
      Show.tenonauger = true
    end
  end
end

function detect_filetype(filename)
  -- assume logs have a unicode box character up top on the first line
  local infile = App.open_for_reading(filename)
  if infile == nil then
    error("file '"..filename.."' not found")
  end
  for line in infile:lines() do
    if line:find('\u{250c}') or line:find('\u{2518}') then
      return 'tenonauger'
    else
      return 'spokecone'
    end
  end
  return 'spokecone'
end

-- tenon auger side is already open; expand spokecone side as well
function expand_spokecone()
  -- temporarily disable resize
--?   print('disabling resize')
  love.handlers.resize = function() end
  Restore_resize_at = App.getTime() + 0.5
  -- use whole window
  App.screen.height = Display_height-100
  App.screen.width = Display_width
--?   App.screen.width = Display_width-100  -- for when I want to see prints on a window below
  App.screen.flags.resizable = true
  App.screen.flags.minwidth = math.min(App.screen.width, 200)
  App.screen.flags.minheight = math.min(App.screen.width, 200)
  love.window.setMode(App.screen.width, App.screen.height, App.screen.flags)
  --
  tenonauger.make_room_for_spokecone()
end

-- environment for a mutable file of bifolded text
-- TODO: some initialization is also happening in load_settings/initialize_default_settings. Clean that up.
function initialize_spokecone(arg)
  if #arg > 0 then
    Editor_state.filename = arg[1]
    load_from_disk(Editor_state)
    Text.redraw_all(Editor_state)
    Editor_state.screen_top1 = {line=1, pos=1}
    Editor_state.cursor1 = {line=1, pos=1}
  else
    load_from_disk(Editor_state)
    Text.redraw_all(Editor_state)
  end
  love.window.setTitle('spokecone.love - '..Editor_state.filename)

  if #arg > 1 then
    print('ignoring commandline args after '..arg[1])
  end

  -- We currently start out with side B collapsed.
  -- Other options:
  --  * save all expanded state by line
  --  * expand all if any location is in side B
  if Editor_state.cursor1.line > #Editor_state.lines then
    Editor_state.cursor1 = {line=1, pos=1}
  end
  if Editor_state.screen_top1.line > #Editor_state.lines then
    Editor_state.screen_top1 = {line=1, pos=1}
  end
  edit.eradicate_locations_after_the_fold(Editor_state)

  if rawget(_G, 'jit') then
    jit.off()
    jit.flush()
  end
end

function load_settings()
  local settings = json.decode(love.filesystem.read('config'))
  love.graphics.setFont(love.graphics.newFont(settings.font_height))
  -- maximize window to determine maximum allowable dimensions
  love.window.setMode(0, 0)  -- maximize
  Display_width, Display_height, App.screen.flags = love.window.getMode()
  -- set up desired window dimensions
  App.screen.flags.resizable = true
  App.screen.flags.minwidth = math.min(App.screen.width, 200)
  App.screen.flags.minheight = math.min(App.screen.width, 200)
  App.screen.width, App.screen.height = settings.width, settings.height
  love.window.setMode(App.screen.width, App.screen.height, App.screen.flags)
  love.window.setPosition(settings.x, settings.y, settings.displayindex)
  Editor_state = edit.initialize_state(Margin_top, Margin_left, App.screen.width-Margin_right, settings.font_height, math.floor(settings.font_height*1.3))
  Editor_state.filename = settings.filename
  Editor_state.screen_top1 = settings.screen_top
  Editor_state.cursor1 = settings.cursor
end

function initialize_default_settings()
  local font_height = 20
  love.graphics.setFont(love.graphics.newFont(font_height))
  local em = App.newText(love.graphics.getFont(), 'm')
  initialize_window_geometry(App.width(em))
  Editor_state = edit.initialize_state(Margin_top, Margin_left, App.screen.width-Margin_right)
  Editor_state.font_height = font_height
  Editor_state.line_height = math.floor(font_height*1.3)
  Editor_state.em = em
end

function initialize_window_geometry(em_width)
  -- maximize window
  love.window.setMode(0, 0)  -- maximize
  Display_width, Display_height, App.screen.flags = love.window.getMode()
  -- shrink height slightly to account for window decoration
  App.screen.height = Display_height-100
  App.screen.width = 40*em_width
  App.screen.flags.resizable = true
  App.screen.flags.minwidth = math.min(App.screen.width, 200)
  App.screen.flags.minheight = math.min(App.screen.width, 200)
  love.window.setMode(App.screen.width, App.screen.height, App.screen.flags)
end

function App.resize(w, h)
--?   print(("Window resized to width: %d and height: %d."):format(w, h))
  App.screen.width, App.screen.height = w, h
  Text.redraw_all(Editor_state)
  Editor_state.selection1 = {}  -- no support for shift drag while we're resizing
  Editor_state.right = App.screen.width-Margin_right
  Editor_state.width = Editor_state.right-Editor_state.left
  Text.tweak_screen_top_and_cursor(Editor_state, Editor_state.left, Editor_state.right)
  Last_resize_time = App.getTime()
--?   print('end resize')
end

function App.filedropped(file)
  -- first make sure to save edits on any existing file
  if Editor_state.next_save then
    save_to_disk(Editor_state)
  end
  -- clear the slate for the new file
  Editor_state.filename = file:getFilename()
  file:open('r')
  Editor_state.lines = load_from_file(file)
  file:close()
  Text.redraw_all(Editor_state)
  Editor_state.screen_top1 = {line=1, pos=1}
  Editor_state.cursor1 = {line=1, pos=1}
  love.window.setTitle('spokecone.love - '..Editor_state.filename)
end

-- a copy of App.filedropped when given a filename
function edit.switch_to_file(filename)
  -- first make sure to save edits on any existing file
  if Editor_state.next_save then
    save_to_disk(Editor_state)
  end
  -- clear the slate for the new file
  Editor_state.filename = filename
  load_from_disk(Editor_state)
  Text.redraw_all(Editor_state)
  Editor_state.screen_top1 = {line=1, pos=1}
  Editor_state.cursor1 = {line=1, pos=1}
end

function App.draw()
  if Show.spokecone then
    edit.draw(Editor_state)
  end
  if Show.tenonauger then
    tenonauger.draw(Tenonauger_state)
  end
  if Show.spokecone and Show.tenonauger then
    -- divider
    love.graphics.rectangle('fill', App.screen.width/2-1,0, 3,App.screen.height)
  end
end

function App.update(dt)
  Cursor_time = Cursor_time + dt
  -- restore resize if it was disabled
  if Restore_resize_at and App.getTime() > Restore_resize_at then
--?     print('restoring resize')
    love.handlers.resize = App.resize
    Restore_resize_at = nil
  end
  -- some hysteresis while resizing
  if App.getTime() < Last_resize_time + 0.1 then
    return
  end
  if Focus == 'spokecone' then
    edit.update(Editor_state, dt)
  else
    tenonauger.update(Tenonauger_state, dt)
  end
end

function love.quit()
  if Focus == 'spokecone' then
    edit.quit(Editor_state)
  else
    tenonauger.quit(Tenonauger_state)
  end
  -- save some important settings
  local x,y,displayindex = love.window.getPosition()
  local filename = Editor_state.filename
  if filename:sub(1,1) ~= '/' then
    filename = love.filesystem.getWorkingDirectory()..'/'..filename  -- '/' should work even on Windows
  end
  local settings = {
    x=x, y=y, displayindex=displayindex,
    width=App.screen.width, height=App.screen.height,
    font_height=Editor_state.font_height,
    filename=filename,
    screen_top=Editor_state.screen_top1, cursor=Editor_state.cursor1}
  love.filesystem.write('config', json.encode(settings))
end

function App.mousepressed(x,y, mouse_button)
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
--?   print('mouse click', x, y)
--?   print(Editor_state.left, Editor_state.right)
--?   print(Tenonauger_state.left, Tenonauger_state.right)
  if Show.spokecone and Editor_state.left <= x and x < Editor_state.right then
--?     print('click on spokecone side')
    if Focus ~= 'spokecone' then
      Focus = 'spokecone'
    else
      edit.mouse_pressed(Editor_state, x,y, mouse_button)
    end
  elseif Show.tenonauger and Tenonauger_state.left <= x and x < Tenonauger_state.right then
--?     print('click on tenonauger side')
    if Focus ~= 'tenonauger' then
      Focus = 'tenonauger'
    else
      return tenonauger.mouse_pressed(Tenonauger_state, x,y, mouse_button)
    end
  end
end

function App.mousereleased(x,y, mouse_button)
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  if Focus == 'spokecone' then
    return edit.mouse_released(Editor_state, x,y, mouse_button)
  else
    return tenonauger.mouse_released(Tenonauger_state, x,y, mouse_button)
  end
end

function App.focus(in_focus)
  if in_focus then
    Last_focus_time = App.getTime()
  end
end

function App.textinput(t)
  -- ignore events for some time after window in focus
  if App.getTime() < Last_focus_time + 0.01 then
    return
  end
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  if Focus == 'spokecone' then
    return edit.textinput(Editor_state, t)
  else
    return tenonauger.textinput(Tenonauger_state, t)
  end
end

function App.keychord_pressed(chord, key)
  -- ignore events for some time after window in focus
  if App.getTime() < Last_focus_time + 0.01 then
    return
  end
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  if Focus == 'spokecone' then
    return edit.keychord_pressed(Editor_state, chord, key)
  else
    return tenonauger.keychord_pressed(Tenonauger_state, chord, key)
  end
end

function App.keyreleased(key, scancode)
  -- ignore events for some time after window in focus
  if App.getTime() < Last_focus_time + 0.01 then
    return
  end
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  if Focus == 'spokecone' then
    return edit.key_released(Editor_state, key, scancode)
  else
    return tenonauger.keychord_pressed(Tenonauger_state, chordkey, scancode)
  end
end

-- use this sparingly
function to_text(s)
  if Text_cache[s] == nil then
    Text_cache[s] = App.newText(love.graphics.getFont(), s)
  end
  return Text_cache[s]
end

-- use this sparingly
function to_text(s)
  if Text_cache[s] == nil then
    Text_cache[s] = App.newText(love.graphics.getFont(), s)
  end
  return Text_cache[s]
end
